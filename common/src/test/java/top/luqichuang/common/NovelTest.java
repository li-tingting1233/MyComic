package top.luqichuang.common;

import org.junit.Test;

import top.luqichuang.common.model.Source;
import top.luqichuang.common.tst.BaseSourceTest;
import top.luqichuang.mynovel.model.NovelInfo;
import top.luqichuang.mynovel.source.AiYue;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/7/9 17:25
 * @ver 1.0
 */
public class NovelTest extends BaseSourceTest<NovelInfo> {
    @Override
    protected Source<? extends NovelInfo> getSource() {
        return new AiYue();
    }

    @Test
    @Override
    public void testRequest() {
        String dRequest = "https://www.biqupai.com/78_78513/";
        String cRequest = "https://www.biqupai.com/78_78513/122951.html";

//        testSearchRequest();
//        testSearch();
//        testDetailRequest(dRequest);
//        testDetail();
//        testContentRequest(cRequest);
//        testContent();
//        testRankMap();
//        testRankRequest();
//        testRank();

//        autoTest();

        allTest();
    }
}
