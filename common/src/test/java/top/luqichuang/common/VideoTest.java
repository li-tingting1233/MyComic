package top.luqichuang.common;

import org.junit.Test;

import top.luqichuang.common.model.Source;
import top.luqichuang.common.tst.BaseSourceTest;
import top.luqichuang.myvideo.model.VideoInfo;
import top.luqichuang.myvideo.source.FengChe2;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/7/9 17:26
 * @ver 1.0
 */
public class VideoTest extends BaseSourceTest<VideoInfo> {
    @Override
    protected Source<? extends VideoInfo> getSource() {
        return new FengChe2();
    }

    @Test
    @Override
    public void testRequest() {

//        testContentRequest("https://www.iyunys.com/503434/18/34.html");
//        testContentRequest("https://www.92wuc.com/player/156-1-1.html");
//        autoTest();

        allTest();

//        testRankMap();

//        testSearchRequest("我的");
    }
}
