package top.luqichuang.mynovel.source;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.model.Content;
import top.luqichuang.mynovel.model.BaseNovelSource;
import top.luqichuang.mynovel.model.NovelInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/2/14 20:05
 * @ver 1.0
 */
@Deprecated
public class QuanXiaoShuo extends BaseNovelSource {
    @Override
    public NSourceEnum getNSourceEnum() {
        return NSourceEnum.QUAN_XIAO_SHUO;
    }

    @Override
    public String getIndex() {
        return "https://qxs.la";
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Request getSearchRequest(String searchString) {
        return null;
    }

    @Override
    public List<NovelInfo> getInfoList(String html) {
        return null;
    }

    @Override
    public void setInfoDetail(NovelInfo info, String html, Map<String, Object> map) {

    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        return null;
    }

    @Override
    public Map<String, String> getRankMap() {
        return null;
    }

    @Override
    public List<NovelInfo> getRankInfoList(String html) {
        return null;
    }
}