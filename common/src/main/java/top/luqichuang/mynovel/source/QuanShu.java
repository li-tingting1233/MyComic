package top.luqichuang.mynovel.source;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.model.Content;
import top.luqichuang.mynovel.model.BaseNovelSource;
import top.luqichuang.mynovel.model.NovelInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/2/12 20:02
 * @ver 1.0
 */
@Deprecated
public class QuanShu extends BaseNovelSource {

    @Override
    public NSourceEnum getNSourceEnum() {
        return NSourceEnum.QUAN_SHU;
    }

    @Override
    public String getIndex() {
        return "http://www.quanshuwang.com";
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Request getSearchRequest(String searchString) {
        return null;
    }

    @Override
    public List<NovelInfo> getInfoList(String html) {
        return null;
    }

    @Override
    public void setInfoDetail(NovelInfo info, String html, Map<String, Object> map) {

    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        return null;
    }

    @Override
    public Map<String, String> getRankMap() {
        return null;
    }

    @Override
    public List<NovelInfo> getRankInfoList(String html) {
        return null;
    }
}