package top.luqichuang.mycomic.model;

import java.util.HashMap;
import java.util.Map;

import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.NetUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 15:26
 * @ver 1.0
 */
public abstract class BaseComicSource implements Source<ComicInfo> {

    public abstract CSourceEnum getSourceEnum();

    @Override
    public final int getSourceId() {
        return getSourceEnum().ID;
    }

    @Override
    public final String getSourceName() {
        return getSourceEnum().NAME;
    }

    @Override
    public Map<String, String> getImageHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Referer", getIndex());
        headers.put("User-Agent", NetUtil.USER_AGENT_WEB);
        return headers;
    }
}
