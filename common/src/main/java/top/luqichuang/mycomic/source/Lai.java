package top.luqichuang.mycomic.source;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;
import top.luqichuang.mycomic.model.BaseComicSource;
import top.luqichuang.mycomic.model.ComicInfo;

/**
 * @author 18472
 * @desc
 * @date 2023/1/27 14:01
 * @ver 1.0
 */
public class Lai extends BaseComicSource {

    @Override
    public CSourceEnum getSourceEnum() {
        return CSourceEnum.LAI;
    }

    @Override
    public String getIndex() {
        return "https://www.laimanhua8.com";
    }

    @Override
    public String getCharsetName(String tag) {
        return "GB2312";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/cse1/search/?key=%s", getIndex(), DecryptUtil.getGBKEncodeStr(searchString));
        return NetUtil.postRequest(url, "", "");
    }

    @Override
    public List<ComicInfo> getInfoList(String html) {
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.ownText("dt a");
                String author = null;
                String updateTime = node.text("dd span");
                String imgUrl = node.src("img");
                String detailUrl = getIndex() + node.href("a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime);
            }
        };
        return starter.startElements(html, "div#dmList li");
    }

    @Override
    public void setInfoDetail(ComicInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("div.title h1");
                String imgUrl = node.src("div.info_cover img");
                String author = node.ownText("div.info p", 1);
                String intro = node.ownText("div#intro1 p");
                String updateStatus = null;
                String updateTime = node.ownText("div.info p span");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, starter.startElements(html, "div#play_0 li"));
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        List<String> urlList = new ArrayList<>();
        try {
            String server = "https://mhpic789-5.kingwar.cn";
            String picTree = StringUtil.match("var picTree ='(.*?)';", html);
            picTree = DecryptUtil.decryptBase64(picTree);
            String[] pics = picTree.split("\\$qingtiandy\\$");
            for (String pic : pics) {
                urlList.add(server + pic);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return SourceHelper.getContentList(urlList, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<ul class=\"nav_menu\"><li><a href=\"/kanmanhua/rexue/\"title=\"热血\">少年热血</a></li><li><a href=\"/kanmanhua/gedou/\"title=\"格斗\">武侠格斗</a></li><li><a href=\"/kanmanhua/kehuan/\"title=\"科幻\">科幻魔幻</a></li><li><a href=\"/kanmanhua/jingji/\"title=\"竞技\">竞技体育</a></li><li><a href=\"/kanmanhua/gaoxiao/\"title=\"搞笑\">爆笑喜剧</a></li><li><a href=\"/kanmanhua/tuili/\"title=\"推理\">侦探推理</a></li><li><a href=\"/kanmanhua/kongbu/\"title=\"恐怖\">恐怖灵异</a></li><li><a href=\"/kanmanhua/danmei/\"title=\"耽美\">耽美人生</a></li><li><a href=\"/kanmanhua/shaonv/\"title=\"少女爱情\">少女</a></li><li><a href=\"/kanmanhua/lianai/\"title=\"恋爱生活\">恋爱</a></li><li><a href=\"/kanmanhua/shenghuo/\"title=\"生活漫画\">生活</a></li></ul>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<ComicInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}
