package top.luqichuang.mycomic.source;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.mycomic.model.BaseComicSource;
import top.luqichuang.mycomic.model.ComicInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/8/3 11:38
 * @ver 1.0
 */
@Deprecated
public class HaoMan6 extends BaseComicSource {

    @Override
    public CSourceEnum getSourceEnum() {
        return CSourceEnum.HAO_MAN_6;
    }

    @Override
    public String getIndex() {
        return "https://www.haoman6.com";
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = getIndex() + "/index.php/search?key=" + searchString;
        return NetUtil.getRequest(url);
    }

    @Override
    public List<ComicInfo> getInfoList(String html) {
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.ownText("p.comic__title a");
                String author = null;
                String updateTime = null;
                String imgUrl = node.attr("img", "data-original");
                String detailUrl = getIndex() + node.href("a");
                try {
                    title = title.replace("(最新在线)", "");
                    if (title.endsWith("-")) {
                        title = title.substring(0, title.length() - 1);
                    }
                } catch (Exception ignored) {
                }
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime);
            }
        };
        return starter.startElements(html, "div.cate-comic-list div.common-comic-item");
    }

    @Override
    public void setInfoDetail(ComicInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("p.comic-title");
                String imgUrl = node.src("img.lazy");
                String author = node.ownText("span.name a");
                String intro = node.ownText("p.intro");
                String updateStatus = null;
                String updateTime = null;
                try {
                    title = title.replace("(最新在线)", "");
                    if (title.endsWith("-")) {
                        title = title.substring(0, title.length() - 1);
                    }
                } catch (Exception ignored) {
                }
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, starter.startElements(html, "ul.chapter__list-box li.j-chapter-item"));
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String[] urls = null;
        JsoupNode node = new JsoupNode(html);
        try {
            Elements elements = node.getElements("div.rd-article-wr div");
            urls = new String[elements.size()];
            for (int i = 0; i < urls.length; i++) {
                node.init(elements.get(i));
                urls[i] = node.attr("img", "data-original");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return SourceHelper.getContentList(urls, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<ul class=\"cate-list clearfix\">\t<li class=\"cate-item\"><a href=\"/category/tags/6\">热血</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/7\">冒险</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/8\">科幻</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/9\">霸总</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/10\">玄幻</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/11\">校园</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/12\">修真</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/13\">搞笑</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/14\">穿越</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/15\">后宫</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/16\">耽美</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/17\">恋爱</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/18\">悬疑</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/19\">恐怖</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/20\">战争</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/21\">动作</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/22\">同人</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/23\">竞技</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/24\">励志</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/25\">架空</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/26\">灵异</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/27\">百合</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/28\">古风</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/29\">生活</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/30\">真人</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/31\">都市</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/48\">其他</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/50\">内地</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/51\">彩虹</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/52\">奇幻</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/53\">猎奇</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/54\">精品</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/55\">推理</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/57\">少女</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/58\">虐恋</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/59\">日更</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/60\">爆笑</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/61\">武侠</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/62\">异能</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/63\">漫改</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/64\">仙侠</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/65\"></a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/67\">萌系</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/68\">日常</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/70\">烧脑</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/71\">腹黑</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/72\">新作</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/73\">神魔</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/74\">运动</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/75\">社会</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/76\">治愈</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/77\">言情</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/78\">震撼</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/81\">总裁</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/82\">剧情</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/105\">古装</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/405\">日漫</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/626\">游戏</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/627\">少年</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/628\">虐心</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/629\">女尊</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/630\">纯爱</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/631\">美型</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/632\">校花</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/634\">金手指</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/635\">蔷薇</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/636\">幽默</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/637\">经典</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/638\">异能 奇幻</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/639\">逆袭 热血</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/640\">其它</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/641\">韩国</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/688\">32</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/689\">38</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/690\">42</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/700\">系统 逆袭</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/701\">逆袭 异能</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/702\">后宫 重生</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/703\">穿越 奇幻</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/704\">异形 重生</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/706\">爱情</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/707\">大女主 重生</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/708\">系统 古风</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/709\">纯爱 古风</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/710\">大女主 剧情</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/711\">恋爱 青春</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/712\">魔法</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/713\">轻小说</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/715\">TS</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/716\">职场</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/717\">修仙</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/718\">奇幻冒险</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/719\">题材:</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/720\">大女主</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/721\">战斗</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/724\">连载</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/723\">惊奇</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/722\">改编</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/725\">连载</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/726\">魔幻</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/727\">怪物</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/728\">长条</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/729\">权谋</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/730\">逆袭</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/731\">复仇</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/732\">系统</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/733\">末日</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/734\">宫斗</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/736\">明星</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/735\">历史</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/737\">青春</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/738\">唯美</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/739\">浪漫</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/740\">美食</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/741\">橘味</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/742\">女生</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/743\">韩漫</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/744\">格斗</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/745\">惊险</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/746\">神仙</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/747\">高甜</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/748\">机甲</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/749\">妖怪</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/750\">知音漫客</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/751\">BL</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/752\">漫客栈</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/753\">脑洞</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/754\">偶像</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/755\">情感</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/756\">日本</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/757\">男生</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/758\">豪快</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/759\">武侠格斗</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/760\">青年</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/761\">冒险热血</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/762\">致郁</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/763\">电竞</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/764\">美女</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/765\">飒漫画</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/766\">惊悚</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/767\">完结</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/768\">甜宠</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/783\">四格</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/785\">召唤</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/786\">异界</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/787\">正能量</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/788\">国漫</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/790\">条漫</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/791\">宠婚</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/800\">重生</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/801\">鬼怪</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/802\">枪战</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/803\">婚宠</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/804\">短篇</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/805\">LOL</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/807\">现代</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/808\">异世</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/809\">血族</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/810\">大女主 恋爱</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/811\">异形</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/814\">玄幻科幻</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/815\">侦探推理</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/817\">古风穿越</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/818\">萌宠</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/819\">幽默搞笑</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/821\">亲情</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/822\">宅斗</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/824\">体育</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/825\">栏目</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/826\">橘系</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/827\">歌舞</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/828\">图谱</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/829\">投稿</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/832\">暖萌</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/833\">宫廷</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/834\">福利</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/835\">神豪</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/967\">玄幻/奇幻/魔幻</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/968\">武侠/古风/后宫</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/969\">吸血</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/971\">机战</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/972\">宠物</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/973\">忍者</a></li>\t<li class=\"cate-item\"><a href=\"/category/tags/974\">小说改编</a></li></ul>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<ComicInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}