package top.luqichuang.mycomic.source;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.json.JsonNode;
import top.luqichuang.common.json.JsonStarter;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;
import top.luqichuang.mycomic.model.BaseComicSource;
import top.luqichuang.mycomic.model.ComicInfo;

/**
 * @author 18472
 * @desc
 * @date 2023/1/27 14:09
 * @ver 1.0
 */
public class MH6 extends BaseComicSource {

    @Override
    public CSourceEnum getSourceEnum() {
        return CSourceEnum.MH_6;
    }

    @Override
    public String getIndex() {
        return "http://www.sixmanhua.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search.php?keyword=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }


    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (DETAIL.equals(tag) && map.isEmpty()) {
            if (data.get("oUrl") == null) {
                data.put("oUrl", data.get("url"));
                String id = null;
                try {
                    String url = (String) data.get("url");
                    url = StringUtil.remove(url, getIndex());
                    id = StringUtil.remove(url, "/");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return NetUtil.postRequest(getIndex() + "/bookchapter/", "id", id, "id2", "1");
            } else {
                map.put("moreChapter", html);
                map.put("url", data.get("oUrl"));
                return NetUtil.getRequest((String) data.get("oUrl"));
            }
        }
        return null;
    }

    @Override
    public List<ComicInfo> getInfoList(String html) {
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.ownText("li.title a");
                String author = null;
                String updateTime = null;
                String imgUrl = node.src("img");
                String detailUrl = getIndex() + node.href("a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime);
            }
        };
        return starter.startElements(html, "div.cy_list_mh ul");
    }

    @Override
    public void setInfoDetail(ComicInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("div.cy_title h1");
                String imgUrl = node.src("img.pic");
                String author = node.ownText("div.cy_xinxi span");
                String intro = node.ownText("p#comic-description");
                String updateStatus = node.ownText("div.cy_xinxi font");
                String updateTime = node.ownText("div.cy_zhangjie_top font");
                author = StringUtil.remove(author, "作者：");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a p");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        List<ChapterInfo> list = starter.startElements(html, "ul#mh-chapter-list-ol-0 li");
        String moreChapter = (String) map.get("moreChapter");
        if (moreChapter != null) {
            JsonStarter<ChapterInfo> jsonStarter = new JsonStarter<ChapterInfo>() {
                @Override
                protected ChapterInfo dealDataList(JsonNode node) {
                    String title = node.string("chaptername");
                    String chapterUrl = map.get("url") + node.string("chapterid") + ".html";
                    return new ChapterInfo(title, chapterUrl);
                }
            };
            list.addAll(jsonStarter.startDataList(moreChapter));
        }
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, list);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String[] urls = null;
        try {
            String js = StringUtil.matchLast("(eval\\(function.*?\\{\\}\\)\\))", html);
            js = DecryptUtil.decryptPackedJsCode(js);
            urls = StringUtil.matchArray("\"(.*?)\"", js);
            if (urls != null) {
                for (int i = 0; i < urls.length; i++) {
                    if (urls[i] != null) {
                        urls[i] = urls[i].replace("\\u0026", "&");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return SourceHelper.getContentList(urls, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<ul><li class=\"\"><a href=\"/rank/1-1.html\">人气榜</a></li><li class=\"\"><a href=\"/rank/2-1.html\">周读榜</a></li><li class=\"\"><a href=\"/rank/3-1.html\">月读榜</a></li><li class=\"\"><a href=\"/rank/4-1.html\">火爆榜</a></li><li class=\"\"><a href=\"/rank/5-1.html\">更新榜</a></li><li class=\"\"><a href=\"/rank/6-1.html\">新慢榜</a></li><li class=\"\"><a href=\"/sort/1-1.html\">冒险热血</a></li><li class=\"\"><a href=\"/sort/2-1.html\">武侠格斗</a></li><li class=\"\"><a href=\"/sort/3-1.html\">科幻魔幻</a></li><li class=\"\"><a href=\"/sort/4-1.html\">侦探推理</a></li><li class=\"\"><a href=\"/sort/5-1.html\">耽美爱情</a></li><li class=\"\"><a href=\"/sort/6-1.html\">生活漫画</a></li><li class=\"\"><a href=\"/sort/12-1.html\">完结漫画</a></li><li class=\"\"><a href=\"/sort/13-1.html\">连载漫画</a></li></ul>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<ComicInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}
