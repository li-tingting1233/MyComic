package top.luqichuang.common.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 9:15
 * @ver 1.0
 */
public class Content {

    private int id;

    private int chapterId;

    private int cur;

    private int total;

    private String url;

    private String content;

    private Map<String, String> headerMap = new HashMap<>();

    private int status;

    public Content() {
    }

    public Content(int chapterId) {
        this.chapterId = chapterId;
        this.cur = 1;
        this.total = 1;
    }

    public Content(int chapterId, String content) {
        this.chapterId = chapterId;
        this.content = content;
    }

    public Content(int chapterId, int cur, int total, String url) {
        this.chapterId = chapterId;
        this.cur = cur;
        this.total = total;
        this.url = url;
    }

    @Override
    public String toString() {
        return "Content{" +
                "id=" + id +
                ", chapterId=" + chapterId +
                ", url='" + url + '\'' +
                ", content='" + content + '\'' +
                ", cur=" + cur +
                ", total=" + total +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Content content = (Content) o;
        return id == content.id && chapterId == content.chapterId && cur == content.cur && total == content.total;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, chapterId, cur, total);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public int getCur() {
        return cur;
    }

    public void setCur(int cur) {
        this.cur = cur;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Map<String, String> getHeaderMap() {
        return headerMap;
    }

    public void setHeaderMap(Map<String, String> headerMap) {
        this.headerMap = headerMap;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
