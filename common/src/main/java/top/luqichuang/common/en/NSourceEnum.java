package top.luqichuang.common.en;

import java.util.LinkedHashMap;
import java.util.Map;

import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.mynovel.source.AiYue;
import top.luqichuang.mynovel.source.K17;
import top.luqichuang.mynovel.source.MiKanShu;
import top.luqichuang.mynovel.source.MoYuan;
import top.luqichuang.mynovel.source.QuanShu;
import top.luqichuang.mynovel.source.QuanXiaoShuo;
import top.luqichuang.mynovel.source.ShuBen;
import top.luqichuang.mynovel.source.TaDu;
import top.luqichuang.mynovel.source.XiaoShuoE;
import top.luqichuang.mynovel.source.XinBiQuGe;
import top.luqichuang.mynovel.source.XinBiQuGe2;
import top.luqichuang.mynovel.source.XuanShu;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/1/11 23:51
 * @ver 1.0
 */
public enum NSourceEnum {
    XIN_BI_QU_GE(1, "新笔趣阁", new XinBiQuGe()),
    QUAN_SHU(2, "全书网", new QuanShu()),
    QUAN_XIAO_SHUO(3, "全小说", new QuanXiaoShuo()),
    AI_YUE(4, "爱阅小说", new AiYue()),
    XUAN_SHU(5, "炫书网", new XuanShu()),
    K17(6, "17K小说", new K17()),
    XIAO_SHUO_E(7, "E小说", new XiaoShuoE()),
    MO_YUAN(8, "墨缘文学", new MoYuan()),
    MI_KAN_SHU(9, "Mi看书", new MiKanShu()),
    XIN_BI_QU_GE_2(10, "新笔趣阁[2]", new XinBiQuGe2()),
    SHU_BEN(11, "书本网", new ShuBen()),
    TA_DU(12, "塔读文学", new TaDu()),
    ;

    private static final Map<Integer, Source<? extends EntityInfo>> MAP = new LinkedHashMap<>();

    static {
        for (NSourceEnum value : values()) {
            if (value.SOURCE.isValid()) {
                MAP.put(value.ID, value.SOURCE);
            }
        }
    }

    public static Map<Integer, Source<? extends EntityInfo>> getMAP() {
        return MAP;
    }

    public final int ID;
    public final String NAME;
    public final Source<? extends EntityInfo> SOURCE;

    NSourceEnum(int id, String name, Source<? extends EntityInfo> source) {
        this.ID = id;
        this.NAME = name;
        this.SOURCE = source;
    }
}
