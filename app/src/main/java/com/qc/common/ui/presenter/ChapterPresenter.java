package com.qc.common.ui.presenter;

import com.qc.common.ui.view.ChapterView;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.SourceUtil;

import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.Request;
import the.one.base.ui.presenter.BasePresenter;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.self.CommonCallback;
import top.luqichuang.common.util.NetUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 17:52
 * @ver 1.0
 */
public class ChapterPresenter extends BasePresenter<ChapterView> {

    public void load(Entity entity) {
        Source<EntityInfo> source = SourceUtil.getSource(entity.getSourceId());
        EntityInfo info = entity.getInfo();
        Request request = source.getDetailRequest(info.getDetailUrl());
        NetUtil.startLoad(new CommonCallback(request, source, Source.DETAIL) {
            @Override
            public void onFailure(String errorMsg) {
                ChapterView view = getView();
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    if (view != null && info.getSourceId() == entity.getInfo().getSourceId()) {
                        view.loadFail();
                    }
                });
            }

            @Override
            public void onResponse(String html, Map<String, Object> map) {
                EntityUtil.setInfoDetail(info, html, map);
                ChapterView view = getView();
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    if (view != null && info.getSourceId() == entity.getInfo().getSourceId()) {
                        if (info.getTitle() != null) {
                            view.loadComplete();
                        } else {
                            view.loadFail();
                        }
                    }
                });
            }
        });
    }

    public void updateSource(Entity entity) {
        List<Source<EntityInfo>> sourceList = SourceUtil.getSourceList();
        for (Source<EntityInfo> source : sourceList) {
            Request request = source.getSearchRequest(entity.getTitle());
            NetUtil.startLoad(new CommonCallback(request, source, Source.SEARCH) {
                @Override
                public void onFailure(String errorMsg) {
                    ChapterView view = getView();
                    AndroidSchedulers.mainThread().scheduleDirect(() -> {
                        if (view != null) {
                            view.updateSourceComplete(null);
                        }
                    });
                }

                @Override
                public void onResponse(String html, Map<String, Object> map) {
                    ChapterView view = getView();
                    AndroidSchedulers.mainThread().scheduleDirect(() -> {
                        if (view != null) {
                            List<EntityInfo> infoList = source.getInfoList(html);
                            view.updateSourceComplete(infoList);
                        }
                    });
                }
            });
        }
    }
}
