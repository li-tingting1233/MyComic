package com.qc.common.ui.adapter;

import android.view.Gravity;

import androidx.annotation.NonNull;

import com.qc.mycomic.R;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButtonDrawable;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundLinearLayout;

import the.one.base.adapter.TheBaseViewHolder;
import top.luqichuang.common.model.Content;

public class ComicReaderAdapter2 extends ComicReaderAdapter {

    @Override
    protected void convert(@NonNull TheBaseViewHolder holder, Content content) {
        QMUIRoundLinearLayout linearLayout = holder.findView(R.id.linearLayout);
        if (linearLayout != null) {
            QMUIRoundButtonDrawable drawable = (QMUIRoundButtonDrawable) linearLayout.getBackground();
            drawable.setStrokeData(0, null);
            linearLayout.setGravity(Gravity.CENTER);
            linearLayout.setMinimumHeight(QMUIDisplayHelper.getScreenHeight(getContext()));
        }
        super.convert(holder, content);
    }

}
