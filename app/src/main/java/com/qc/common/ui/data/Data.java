package com.qc.common.ui.data;

import com.qc.common.en.SettingEnum;
import com.qc.common.util.RestartUtil;
import com.qc.common.util.SettingUtil;

import java.util.List;

import the.one.base.util.SdCardUtil;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/10/30 11:48
 * @ver 1.0
 */
public class Data {

    /*=============================================================================*/
    /* 常量 */
    /* App Path */
    public static final String NORMAL_PATH = SdCardUtil.getNormalSDCardPath();
    public static final String APP_PATH = NORMAL_PATH + "/MyComic";
    public static final String SHELF_IMG_PATH = APP_PATH + "/ShelfImg";
    public static final String IMG_PATH = APP_PATH + "/Image";
    public static final String AUTO_SAVE_PATH = APP_PATH + "/AutoBackup";

    /* 标识App显示的数据内容 */
    public static final int COMIC_CODE = 1;
    public static final int NOVEL_CODE = 2;
    public static final int VIDEO_CODE = 3;

    /* toStatus */
    public static final int NORMAL = 0;
    public static final int READER_TO_CHAPTER = 1;
    public static final int SEARCH_TO_CHAPTER = 2;
    public static final int RANK_TO_CHAPTER = 3;
    public static final int TO_IMPORT = 4;
    public static final int TO_IMPORT_SUCCESS = 5;

    /* SCREEN */
    public static final int SCREEN_0 = 0;
    public static final int SCREEN_1 = 1;

    /* ReaderMode */
    public static final int READER_MODE_V = 0;
    public static final int READER_MODE_H_R = 1;
    public static final int READER_MODE_H_L = 2;

    /*=============================================================================*/
    /* 公共静态变量 */
    public static int toStatus = Data.NORMAL;
    public static boolean isLight = true;
    public static boolean isFull = (boolean) SettingUtil.getSettingKey(SettingEnum.IS_FULL_SCREEN);
    public static int contentCode = (int) SettingUtil.getSettingKey(SettingEnum.READ_CONTENT);
    public static String contentStr = SettingUtil.getSettingDesc(SettingEnum.READ_CONTENT);
    public static int videoSpeed = 2;
    public static int chapterNum = 0;

    /*=============================================================================*/
    /* 私有静态变量 */
    private static Entity entity;
    private static Content content;
    private static List<Content> contentList;

    public static Entity getEntity() {
        if (entity == null) {
            RestartUtil.restart();
        }
        return entity;
    }

    public static Content getContent() {
        if (content == null) {
            RestartUtil.restart();
        }
        return content;
    }

    public static List<Content> getContentList() {
        if (contentList == null) {
            RestartUtil.restart();
        }
        return contentList;
    }

    public static void setEntity(Entity entity) {
        Data.entity = entity;
    }

    public static void setContent(Content content) {
        Data.content = content;
    }

    public static void setContentList(List<Content> contentList) {
        Data.contentList = contentList;
    }
    /*=============================================================================*/
}
