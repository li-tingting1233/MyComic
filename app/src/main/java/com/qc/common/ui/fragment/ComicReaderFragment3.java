package com.qc.common.ui.fragment;

import android.widget.SeekBar;

import com.qc.common.ui.data.Data;

import java.util.List;

import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/11/1 19:09
 * @ver 1.0
 */
public class ComicReaderFragment3 extends ComicReaderFragment2 {

    @Override
    protected void setListener() {
        super.setListener();
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (contentList != null && !contentList.isEmpty() && isSmooth) {
                    int p = contentList.size() - progress - 1;
                    if (checkPosition(p)) {
                        recycleView.scrollToPosition(p);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isSmooth = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isSmooth = false;
            }
        });
    }

    @Override
    protected void preLoad(Content content) {
    }

    @Override
    public void loadReadContentComplete(List<Content> contentList, String errorMsg) {
        if (contentList != null) {
            StringUtil.swapList(contentList);
        }
        super.loadReadContentComplete(contentList, errorMsg);
    }

    @Override
    public void onComplete(List<Content> data) {
        super.onComplete(data);
        if (!adapter.getData().isEmpty()) {
            recycleView.scrollToPosition(adapter.getData().size() - 1 - Data.chapterNum);
            Data.chapterNum = 0;
        }
        adapter.getLoadMoreModule().setEnableLoadMore(false);
    }
}
