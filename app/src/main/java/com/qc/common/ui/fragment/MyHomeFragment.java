package com.qc.common.ui.fragment;

import android.view.View;

import com.qc.common.ui.data.Data;
import com.qc.mycomic.R;

import java.util.ArrayList;

import the.one.base.ui.fragment.BaseFragment;
import the.one.base.ui.fragment.BaseHomeFragment;

/**
 * @author LuQiChuang
 * @desc HOME界面
 * @date 2020/8/12 15:26
 * @ver 1.0
 */
public class MyHomeFragment extends BaseHomeFragment {

    private static final String[] COMIC_TAB_BARS = {
            "我的画架",
            "搜索漫画",
            "个人中心",
    };

    private static final String[] NOVEL_TAB_BARS = {
            "我的书架",
            "搜索小说",
            "个人中心",
    };

    private static final String[] VIDEO_TAB_BARS = {
            "我的番剧",
            "搜索番剧",
            "个人中心",
    };

    public static String[] getTabBars() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return COMIC_TAB_BARS;
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return NOVEL_TAB_BARS;
        } else {
            return VIDEO_TAB_BARS;
        }
    }

    @Override
    protected boolean isExitFragment() {
        return true;
    }

    @Override
    protected boolean isNeedChangeStatusBarMode() {
        return true;
    }

    @Override
    protected boolean isViewPagerSwipe() {
        return false;
    }

    @Override
    protected boolean isDestroyItem() {
        return false;
    }

    @Override
    protected void addTabs() {
        addTab(R.drawable.ic_baseline_home_24, R.drawable.ic_baseline_home_select_24, getTabBars()[0]);
        addTab(R.drawable.ic_baseline_search_24, R.drawable.ic_baseline_search_select_24, getTabBars()[1]);
        addTab(R.drawable.ic_baseline_person_24, R.drawable.ic_baseline_person_select_24, getTabBars()[2]);
    }

    @Override
    protected void addFragment(ArrayList<BaseFragment> fragments) {
        fragments.add(new ShelfFragment());
        fragments.add(new SearchBaseFragment());
        fragments.add(new PersonFragment());
    }

    @Override
    protected void initView(View rootView) {
        super.initView(rootView);
        startInit();
    }

    @Override
    protected void onLazyInit() {
    }
}
