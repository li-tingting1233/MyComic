package com.qc.common.ui.fragment;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qc.common.en.SettingEnum;
import com.qc.common.ui.adapter.SearchAdapter;
import com.qc.common.ui.data.Data;
import com.qc.common.ui.presenter.SearchPresenter;
import com.qc.common.ui.view.SearchView;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.SettingUtil;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.qqface.QMUIQQFaceView;

import java.util.ArrayList;
import java.util.List;

import the.one.base.ui.fragment.BaseDataFragment;
import the.one.base.ui.presenter.BasePresenter;
import the.one.base.util.QMUIDialogUtil;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 15:32
 * @ver 1.0
 */
public class SearchResultFragment extends BaseDataFragment<Entity> implements SearchView {

    private SearchPresenter presenter = new SearchPresenter();

    private List<Entity> entityList = EntityUtil.getEntityList();

    private String searchString;

    public static SearchResultFragment getInstance(String searchString) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle bundle = new Bundle();
        bundle.putString("searchString", searchString);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        this.searchString = (String) getArguments().get("searchString");
        super.onCreate(savedInstanceState);
    }

    @Override
    protected BaseQuickAdapter getAdapter() {
        return new SearchAdapter(R.layout.item_search);
    }

    @Override
    protected void initView(View rootView) {
        super.initView(rootView);
        showLoadingPage();
        QMUIQQFaceView mTitle = mTopLayout.setTitle("搜索结果:" + searchString);
        mTopLayout.setTitleGravity(Gravity.CENTER);
        mTitle.setTextColor(getColorr(R.color.qmui_config_color_gray_1));
        mTitle.getPaint().setFakeBoldText(true);
        addTopBarBackBtn();
        requestServer();
    }

    @Override
    protected void onLazyInit() {
    }

    @Override
    protected void requestServer() {
        count = 0;
        presenter.search(searchString);
        showContentPage();
        if (progressDialog == null) {
            showProgressDialog(getPercent(), total, getMsg());
        } else {
            progressDialog.setProgress(getPercent(), total);
            progressDialog.setMessage(getMsg());
            progressDialog.show();
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_recycle_view;
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
        Entity entity = (Entity) adapter.getData().get(position);
        //Log.i(TAG, "onItemClick: " + entity);
        int index = entityList.indexOf(entity);
        if (index != -1) {
            Entity myEntity = entityList.get(index);
            for (EntityInfo entityInfo : entity.getInfoList()) {
                if (!myEntity.getInfoList().contains(entityInfo)) {
                    EntityUtil.addInfo(myEntity, entityInfo);
                }
            }
            Data.toStatus = Data.SEARCH_TO_CHAPTER;
            Data.setEntity(myEntity);
        } else {
            Data.toStatus = Data.SEARCH_TO_CHAPTER;
            Data.setEntity(entity);
        }
        startFragment(new ChapterFragment());
    }

    @Override
    public boolean onItemLongClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {
        return false;
    }

    @Override
    protected void initAdapter() {
        super.initAdapter();
        adapter.getLoadMoreModule().setOnLoadMoreListener(null);
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    private int count = 0;
    private int total = 100;
    private int size = 0;
    private List<String> errorList = new ArrayList<>();

    @Override
    public void searchComplete(List<Entity> entityList, String sourceName) {
        if (sourceName != null) {
            errorList.add(sourceName);
        }
        if (++count == size) {
            int sourceId;
            if (Data.contentCode == Data.COMIC_CODE) {
                sourceId = (int) SettingUtil.getSettingKey(SettingEnum.DEFAULT_COMIC_SOURCE);
            } else if (Data.contentCode == Data.NOVEL_CODE) {
                sourceId = (int) SettingUtil.getSettingKey(SettingEnum.DEFAULT_NOVEL_SOURCE);
            } else {
                sourceId = (int) SettingUtil.getSettingKey(SettingEnum.DEFAULT_VIDEO_SOURCE);
            }
            for (int i = 0; i < entityList.size(); i++) {
                Entity entity = entityList.get(i);
                EntityUtil.changeInfo(entity, sourceId);
                if (searchString.equals(entity.getTitle()) && i != 0) {
                    Entity tmp = entityList.get(0);
                    entityList.set(0, entity);
                    entityList.set(i, tmp);
                }
            }
            onFirstComplete(entityList);
            adapter.notifyDataSetChanged();
            count = 0;
            hideProgressDialog();
            if (errorList.isEmpty()) {
                showSuccessTips("搜索完毕");
            } else {
                StringBuilder tip = new StringBuilder();
                for (String s : errorList) {
                    tip.append(s).append("\n");
                }
                QMUIDialogUtil.showSimpleDialog(getContext(), "搜索结果", "搜索完毕，失败" + Data.contentStr + "源数：" + errorList.size() + "\n" + tip);
                errorList.clear();
            }
        } else {
            onFirstComplete(entityList);
            adapter.notifyDataSetChanged();
            showProgressDialog(getPercent(), total);
            progressDialog.setMessage(getMsg());
        }
    }

    private int getPercent() {
        checkSize();
        if (size != 0) {
            return count * total / size;
        } else {
            return 100;
        }
    }

    private String getLoadProcess() {
        checkSize();
        return count + "/" + size;
    }

    private String getMsg() {
        return "正在搜索 " + getLoadProcess();
    }

    private void checkSize() {
        if (size == 0) {
            size = SourceUtil.size();
        }
    }
}
