package com.qc.common.ui.presenter;


import com.qc.common.ui.view.SearchView;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.SourceUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.Request;
import the.one.base.ui.presenter.BasePresenter;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.self.CommonCallback;
import top.luqichuang.common.util.NetUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 15:33
 * @ver 1.0
 */
public class SearchPresenter extends BasePresenter<SearchView> {

    private List<Entity> entityList;

    public void search(String searchString) {
        entityList = new ArrayList<>();
        List<Source<EntityInfo>> sourceList = SourceUtil.getSourceList();
        for (Source<EntityInfo> source : sourceList) {
            Request request = source.getSearchRequest(searchString);
            NetUtil.startLoad(new CommonCallback(request, source, Source.SEARCH) {
                @Override
                public void onFailure(String errorMsg) {
                    SearchView view = getView();
                    AndroidSchedulers.mainThread().scheduleDirect(() -> {
                        if (view != null) {
                            view.searchComplete(entityList, source.getSourceName());
                        }
                    });
                }

                @Override
                public void onResponse(String html, Map<String, Object> map) {
                    SearchView view = getView();
                    AndroidSchedulers.mainThread().scheduleDirect(() -> {
                        if (view != null) {
                            List<EntityInfo> infoList = source.getInfoList(html);
                            mergeInfoList(infoList);
                            view.searchComplete(entityList, null);
                        }
                    });
                }
            });
        }
    }

    private void mergeInfoList(List<EntityInfo> infoList) {
        for (EntityInfo entityInfo : infoList) {
            boolean isExists = false;
            for (Entity entity : entityList) {
                if (entity.getTitle().equals(entityInfo.getTitle())) {
                    isExists = true;
                    EntityUtil.addInfo(entity, entityInfo);
                }
            }
            if (!isExists) {
                Entity entity = SourceUtil.getEntity(entityInfo);
                entityList.add(entity);
            }
        }
    }

}
