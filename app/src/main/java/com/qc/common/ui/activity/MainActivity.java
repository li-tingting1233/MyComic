package com.qc.common.ui.activity;

import android.os.Bundle;

import com.qc.common.MyBaseApplication;
import com.qc.common.ui.fragment.MyHomeFragment;
import com.qc.common.util.RestartUtil;

import the.one.base.ui.activity.BaseFragmentActivity;
import the.one.base.ui.fragment.BaseFragment;
import the.one.base.util.SpUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2020/8/12 15:25
 * @ver 1.0
 */
public class MainActivity extends BaseFragmentActivity {

    private static MainActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyBaseApplication.addActivity(this);
        activity = this;
    }

    public static MainActivity getInstance() {
        if (activity == null) {
            SpUtil.getInstance().putBoolean(RestartUtil.NEED_RESTART, true);
        }
        return activity;
    }

    @Override
    protected BaseFragment getFirstFragment() {
        return new MyHomeFragment();
    }

}