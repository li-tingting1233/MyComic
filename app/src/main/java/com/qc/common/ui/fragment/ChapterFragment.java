package com.qc.common.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qc.common.self.ImageConfig;
import com.qc.common.ui.data.Data;
import com.qc.common.ui.presenter.ChapterPresenter;
import com.qc.common.ui.view.ChapterView;
import com.qc.common.util.AnimationUtil;
import com.qc.common.util.DBUtil;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.ImgUtil;
import com.qc.common.util.PopupUtil;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.qqface.QMUIQQFaceView;
import com.qmuiteam.qmui.widget.QMUIRadiusImageView;
import com.qmuiteam.qmui.widget.dialog.QMUIBottomSheet;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import the.one.base.ui.fragment.BaseFragment;
import the.one.base.ui.fragment.BaseTabFragment;
import the.one.base.util.QMUIDialogUtil;
import the.one.base.util.QMUIPopupUtil;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.MapUtil;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/24 12:27
 * @ver 1.0
 */
public class ChapterFragment extends BaseTabFragment implements ChapterView {

    private Entity entity;
    private ChapterPresenter presenter = new ChapterPresenter();
    private int size;

    private QMUIPopup mSettingPopup;
    private String[] mMenus = new String[]{
            "更新" + Data.contentStr + "源",
            "查看信息",
            "访问源网站",
    };
    private ImageButton ibMenu;
    private ImageButton ibSwap;

    private QMUIRadiusImageView imageView;
    private RelativeLayout relativeLayout;
    private TextView tvTitle;
    private TextView tvSource;
    private TextView tvSourceSize;
    private TextView tvUpdateTime;
    private TextView tvUpdateChapter;
    private TextView tvRead;
    private TextView tvReadNum;
    private LinearLayout favLayout;
    private ImageView ivFav;
    private TextView tvFav;
    private View llIndicator;

    public ChapterFragment() {
        this.entity = Data.getEntity();
        this.size = SourceUtil.size();
    }

    @Override
    public void onResume() {
        super.onResume();
        //后退返回此页面时刷新数据
        if (entity != null && !fragments.isEmpty()) {
            for (BaseFragment fragment : fragments) {
                ((ChapterItemFragment) fragment).updateData();
            }
            setValue();
        }
    }

    @Override
    protected boolean isTabFromNet() {
        return true;
    }

    @Override
    protected void initView(View rootView) {
        super.initView(rootView);
        showLoadingPage();
        mMagicIndicator = rootView.findViewById(R.id.indicator);
        mViewPager = rootView.findViewById(R.id.viewPager);

        QMUIQQFaceView mTitle = mTopLayout.setTitle("" + Data.contentStr + "详情");
        mTopLayout.setTitleGravity(Gravity.CENTER);
        mTitle.setTextColor(getColorr(R.color.qmui_config_color_gray_1));
        mTitle.getPaint().setFakeBoldText(true);
        addTopBarBackBtn();
        loadComplete();
        requestServer();
    }

    @Override
    protected void onLazyInit() {
    }

    private void setListener() {
        //菜单按钮: 更新源、查看信息
        ibMenu.setOnClickListener(v -> {
            if (null == mSettingPopup) {
                mSettingPopup = QMUIPopupUtil.createListPop(_mActivity, mMenus, (adapter, view, position) ->
                {
                    if (position == 0) {
                        showProgressDialog(0, size, "正在更新" + Data.contentStr + "源");
                        presenter.updateSource(entity);
                    } else if (position == 1) {
                        QMUIDialogUtil.showSimpleDialog(getContext(), "查看信息", EntityUtil.toStringView(entity)).show();
                    } else if (position == 2) {
                        String url = entity.getInfo().getDetailUrl();
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    mSettingPopup.dismiss();
                });
            }
            mSettingPopup.show(ibMenu);
        });

        //更换顺序
        ibSwap.setOnClickListener(v -> {
            if (checkNotEmpty()) {
                showLoadingPage();
                EntityInfo entityInfo = entity.getInfo();
                entityInfo.setOrder(entityInfo.getOrder() == EntityInfo.ASC ? EntityInfo.DESC : EntityInfo.ASC);
                DBUtil.saveInfoData(entityInfo);
                loadComplete();
            } else {
                showFailTips("暂无" + Data.contentStr + "章节");
            }
        });

        //改变源
        TextView tvSource = mRootView.findViewById(R.id.tvSource);
        tvSource.setOnClickListener(v -> {
            Map<String, String> map = PopupUtil.getMap(entity.getInfoList());
            String key = PopupUtil.getKey(entity);
            PopupUtil.showSimpleBottomSheetList(getContext(), map, key, "切换" + Data.contentStr + "源", new QMUIBottomSheet.BottomListSheetBuilder.OnSheetItemClickListener() {
                @Override
                public void onClick(QMUIBottomSheet dialog, View itemView, int position, String tag) {
                    String key = MapUtil.getKeyByValue(map, tag);
                    String[] ss = key.split("#");
                    if (EntityUtil.changeInfo(entity, ss)) {
                        showLoadingPage();
                        loadComplete();
                        requestServer();
                        DBUtil.save(entity, DBUtil.SAVE_ONLY);
                    }
                    dialog.dismiss();
                }
            });
        });

        //阅读最新章节
        TextView tvUpdateChapter = mRootView.findViewById(R.id.tvUpdateChapter);
        tvUpdateChapter.setOnClickListener(v -> {
            if (checkNotEmpty()) {
                EntityUtil.newestChapter(entity.getInfo());
                ((ChapterItemFragment) fragments.get(INDEX)).start();
            } else {
                showFailTips("暂无" + Data.contentStr + "章节");
            }
        });

        //收藏
        LinearLayout favLayout = mRootView.findViewById(R.id.favLayout);
        favLayout.setOnClickListener(v -> {
            boolean isFav = entity.getStatus() != EntityUtil.STATUS_FAV;
            setFavLayout(isFav, true);
            EntityUtil.removeEntity(entity);
            entity.setStatus(isFav ? EntityUtil.STATUS_FAV : EntityUtil.STATUS_HIS);
            //Log.i(TAG, "setListener: " + entity);
            EntityUtil.first(entity);
        });

        //开始阅读
        LinearLayout llRead = mRootView.findViewById(R.id.llRead);
        llRead.setOnClickListener(v -> {
            if (checkNotEmpty()) {
                if (fragments.size() <= INDEX) {
                    INDEX = 0;
                }
                ((ChapterItemFragment) fragments.get(INDEX)).startId(entity.getInfo().getCurChapterId());
            } else {
                showFailTips("暂无" + Data.contentStr + "章节");
            }
        });

        //阅读下一章
        TextView tvReadNext = mRootView.findViewById(R.id.tvReadNext);
        tvReadNext.setOnClickListener(v -> {
            if (checkNotEmpty()) {
                if (fragments.size() <= INDEX) {
                    INDEX = 0;
                }
                ((ChapterItemFragment) fragments.get(INDEX)).startId(entity.getInfo().getCurChapterId() + 1);
            } else {
                showFailTips("暂无" + Data.contentStr + "章节");
            }
        });
    }

    private void addView() {
        ibMenu = mTopLayout.addRightImageButton(R.drawable.ic_baseline_menu_24, R.id.topbar_right_button1);
        ibSwap = mTopLayout.addRightImageButton(R.drawable.ic_baseline_swap_vert_24, R.id.topbar_right_button2);
        relativeLayout = mRootView.findViewById(R.id.imageRelativeLayout);
        imageView = mRootView.findViewById(R.id.imageView);
        tvTitle = mRootView.findViewById(R.id.tvTitle);
        tvSource = mRootView.findViewById(R.id.tvSource);
        tvSourceSize = mRootView.findViewById(R.id.tvSourceSize);
        tvUpdateTime = mRootView.findViewById(R.id.tvUpdateTime);
        tvUpdateChapter = mRootView.findViewById(R.id.tvUpdateChapter);
        tvRead = mRootView.findViewById(R.id.tvRead);
        tvReadNum = mRootView.findViewById(R.id.tvReadNum);
        favLayout = mRootView.findViewById(R.id.favLayout);
        ivFav = favLayout.findViewById(R.id.ivFav);
        tvFav = favLayout.findViewById(R.id.tvFav);
        llIndicator = mRootView.findViewById(R.id.llIndicator);

        imageView.setOnLongClickListener(v -> {
            ImageConfig config = ImgUtil.getDefaultConfig(getContext(), entity.getInfo().getImgUrl(), relativeLayout);
            config.setForce(true);
            config.setSave(true);
            ImgUtil.setSaveKey(entity, config);
            Source<EntityInfo> source = SourceUtil.getSource(entity.getSourceId());
            config.setHeaders(source.getImageHeaders());
            ImgUtil.loadImage(getContext(), config);
            return false;
        });
    }

    private void setValue() {
        ImageConfig config = ImgUtil.getDefaultConfig(getContext(), entity.getInfo().getImgUrl(), relativeLayout);
        config.setSave(true);
        ImgUtil.setSaveKey(entity, config);
        Source<EntityInfo> source = SourceUtil.getSource(entity.getSourceId());
        config.setHeaders(source.getImageHeaders());
        ImgUtil.loadImage(getContext(), config);
        tvTitle.setText(entity.getInfo().getTitle());
        tvSource.setText(EntityUtil.sourceName(entity));
        tvSourceSize.setText(String.format(Locale.CHINA, "(%d)", EntityUtil.sourceSize(entity)));
        tvUpdateChapter.setText(entity.getInfo().getUpdateChapter());
        tvUpdateTime.setText(entity.getInfo().getUpdateTime());
        if (entity.getCurChapterTitle() != null) {
            tvRead.setText("继续阅读");
            tvReadNum.setText(String.format(Locale.CHINA, "%s - %02d页", entity.getCurChapterTitle(), entity.getInfo().getChapterNum() + 1));
            tvReadNum.setVisibility(View.VISIBLE);
        } else {
            tvRead.setText("开始阅读");
            tvReadNum.setVisibility(View.GONE);
        }
        setFavLayout(entity.getStatus() == EntityUtil.STATUS_FAV);
    }

    public void setFavLayout(boolean isFav) {
        setFavLayout(isFav, false);
    }

    public void setFavLayout(boolean isFav, boolean needAnimation) {
        if (isFav) {
            AnimationUtil.changeDrawable(ivFav, getDrawablee(R.drawable.ic_baseline_favorite_24), needAnimation);
            tvFav.setText("已收藏");
        } else {
            AnimationUtil.changeDrawable(ivFav, getDrawablee(R.drawable.ic_baseline_favorite_border_24), needAnimation);
            tvFav.setText("未收藏");
        }
    }

    private boolean firstLoad = true;

    @Override
    protected void requestServer() {
        itemLoading();
        Map<String, List<ChapterInfo>> map = entity.getInfo().getChapterInfoMap();
        if (map == null || map.size() == 0 || entity.getInfo().getChapterInfoList().isEmpty()) {
            presenter.load(entity);
        } else {
            loadComplete();
        }
    }

    @Override
    public ChapterPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void startInit() {
        mTabs.clear();
        super.startInit();
        Map<String, List<ChapterInfo>> map = entity.getInfo().getChapterInfoMap();
        if (!map.isEmpty() && !entity.getInfo().getChapterInfoList().isEmpty()) {
            int i = 0;
            for (List<ChapterInfo> list : map.values()) {
                ChapterItemFragment fragment = (ChapterItemFragment) fragments.get(i++);
                fragment.setList(list);
            }
        } else {
            loadFail();
        }
    }

    @Override
    public void loadComplete() {
        try {
            List<ChapterInfo> list = entity.getInfo().getChapterInfoList();
            if (isNeedSwap(list, entity.getInfo().getOrder())) {
                StringUtil.swapList(list);
                for (List<ChapterInfo> value : entity.getInfo().getChapterInfoMap().values()) {
                    if (list != value) {
                        StringUtil.swapList(value);
                    }
                }
            }
            if (firstLoad) {
                firstLoad = false;
                addView();
                setListener();
            }
            setValue();
            if (Data.toStatus == Data.RANK_TO_CHAPTER) {
                Data.toStatus = Data.NORMAL;
                showProgressDialog("正在更新" + Data.contentStr + "源");
                presenter.updateSource(entity);
            }
            if (Data.toStatus == Data.TO_IMPORT && entity.getInfo().getTitle() != null) {
                Data.toStatus = Data.NORMAL;
                List<Entity> entityList = EntityUtil.getEntityList();
                entity.setTitle(entity.getInfo().getTitle());
                int index = entityList.indexOf(entity);
                if (index != -1) {
                    Entity e = entityList.get(index);
                    if (!EntityUtil.changeInfo(e, entity.getSourceId())) {
                        EntityUtil.addInfo(e, entity.getInfo());
                    }
                    Data.setEntity(entity);
                    this.entity = entityList.get(index);
                    setValue();
                } else {
                    DBUtil.save(entity);
                    EntityUtil.initEntityList(EntityUtil.STATUS_ALL);
                }
            }
            if (entity.getInfo().getTitle() != null) {
                startInit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadFail() {
        for (int i = 0; i < fragments.size(); i++) {
            fragments.get(i).showEmptyPage("章节加载失败", v -> {
                requestServer();
            });
        }
        if (Data.toStatus == Data.TO_IMPORT) {
            Data.toStatus = Data.NORMAL;
            showEmptyPage("导入失败...", v -> {
                showLoadingPage();
                Data.toStatus = Data.TO_IMPORT;
                requestServer();
            });
        }
    }

    private boolean isNeedSwap(List<ChapterInfo> list, int order) {
        if (list != null && !list.isEmpty()) {
            int firstId = list.get(0).getId();
            int lastId = list.get(list.size() - 1).getId();
            if (firstId > lastId) {
                return order != EntityInfo.DESC;
            } else if (firstId < lastId) {
                return order != EntityInfo.ASC;
            } else {
                return false;
            }
        }
        return false;
    }

    private int count = 0;

    @Override
    public void updateSourceComplete(List<EntityInfo> infoList) {
        count++;
        if (infoList != null) {
            for (EntityInfo info : infoList) {
                updateEntityInfo(info);
            }
        }
        if (count == size) {
            count = 0;
            hideProgressDialog();
            showSuccessTips("搜索完毕");
            setValue();
            DBUtil.save(entity, DBUtil.SAVE_ALL);
        } else {
            progressDialog.setMessage(getMsg("正在更新" + Data.contentStr + "源", count, size));
            progressDialog.setProgress(getValue(count, size), 100);
            tvSourceSize.setText(String.format(Locale.CHINA, "(%d)", EntityUtil.sourceSize(entity)));
        }
    }

    private int getValue(int count, int max) {
        if (max > 0) {
            return count * 100 / max;
        } else {
            return count;
        }
    }

    private String getMsg(String msg, int count, int max) {
        return String.format(Locale.CHINA, "%s %d/%d", msg, count, max);
    }

    private void updateEntityInfo(EntityInfo info) {
        if (!entity.getTitle().equals(info.getTitle())) {
            return;
        }
        int index = entity.getInfoList().indexOf(info);
        if (index == -1) {
            EntityUtil.addInfo(entity, info);
        }
    }

    private boolean checkNotEmpty() {
        return !entity.getInfo().getChapterInfoList().isEmpty();
    }

    @Override
    protected void addTabs() {
        Map<String, List<ChapterInfo>> map = entity.getInfo().getChapterInfoMap();
        if ((map.size() == 1 && map.containsKey("正文")) || map.isEmpty()) {
            llIndicator.setVisibility(View.GONE);
        } else {
            llIndicator.setVisibility(View.VISIBLE);
            for (String key : entity.getInfo().getChapterInfoMap().keySet()) {
                addTab(key);
            }
        }
    }

    @Override
    protected void addFragment(ArrayList<BaseFragment> fragments) {
        if (fragments.isEmpty()) {
            if (!getChildFragmentManager().getFragments().isEmpty()) {
                fragments.addAll((List<BaseFragment>) (List) getChildFragmentManager().getFragments());
            } else {
                fragments.add(new ChapterItemFragment());
            }
        }
        int mapSize = entity.getInfo().getChapterInfoMap().size();
        int fSize = fragments.size();
        if (mapSize > fSize) {
            for (int i = 0; i < mapSize - fSize; i++) {
                ChapterItemFragment fragment = new ChapterItemFragment();
                fragments.add(fragment);
            }
        } else if (mapSize < fSize) {
            if (mapSize > 0) {
                fragments.subList(mapSize, fSize).clear();
            } else {
                fragments.subList(1, fSize).clear();
            }
        }
    }

    private void itemLoading() {
        if (llIndicator != null) {
            llIndicator.setVisibility(View.GONE);
        }
        for (int i = 0; i < fragments.size(); i++) {
            fragments.get(i).showLoadingPage();
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_chapter;
    }
}
