package com.qc.common.util;


import com.qc.common.ui.data.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.en.VSourceEnum;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.MapUtil;
import top.luqichuang.mycomic.model.Comic;
import top.luqichuang.mycomic.model.ComicInfo;
import top.luqichuang.mynovel.model.Novel;
import top.luqichuang.mynovel.model.NovelInfo;
import top.luqichuang.myvideo.model.Video;
import top.luqichuang.myvideo.model.VideoInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 15:51
 * @ver 1.0
 */
public class SourceUtil {

    private static final Map<Integer, Source<? extends EntityInfo>> COMIC_MAP = CSourceEnum.getMAP();
    private static final Map<Integer, Source<? extends EntityInfo>> NOVEL_MAP = NSourceEnum.getMAP();
    private static final Map<Integer, Source<? extends EntityInfo>> VIDEO_MAP = VSourceEnum.getMAP();

    private static final Map<Integer, String> COMIC_NAME_MAP = new HashMap<>();
    private static final Map<Integer, String> NOVEL_NAME_MAP = new HashMap<>();
    private static final Map<Integer, String> VIDEO_NAME_MAP = new HashMap<>();

    private static final List<Source<? extends EntityInfo>> COMIC_SOURCE_LIST = new ArrayList<>();
    private static final List<Source<? extends EntityInfo>> NOVEL_SOURCE_LIST = new ArrayList<>();
    private static final List<Source<? extends EntityInfo>> VIDEO_SOURCE_LIST = new ArrayList<>();

    private static final List<String> COMIC_SOURCE_NAME_LIST = new ArrayList<>();
    private static final List<String> NOVEL_SOURCE_NAME_LIST = new ArrayList<>();
    private static final List<String> VIDEO_SOURCE_NAME_LIST = new ArrayList<>();

    static {
        for (Source<? extends EntityInfo> source : COMIC_MAP.values()) {
            COMIC_NAME_MAP.put(source.getSourceId(), source.getSourceName());
            COMIC_SOURCE_LIST.add(source);
            COMIC_SOURCE_NAME_LIST.add(source.getSourceName());
        }
        for (Source<? extends EntityInfo> source : NOVEL_MAP.values()) {
            NOVEL_NAME_MAP.put(source.getSourceId(), source.getSourceName());
            NOVEL_SOURCE_LIST.add(source);
            NOVEL_SOURCE_NAME_LIST.add(source.getSourceName());
        }
        for (Source<? extends EntityInfo> source : VIDEO_MAP.values()) {
            VIDEO_NAME_MAP.put(source.getSourceId(), source.getSourceName());
            VIDEO_SOURCE_LIST.add(source);
            VIDEO_SOURCE_NAME_LIST.add(source.getSourceName());
        }
    }

    public static void init() {
        COMIC_SOURCE_LIST.clear();
        NOVEL_SOURCE_LIST.clear();
        VIDEO_SOURCE_LIST.clear();
        COMIC_SOURCE_LIST.addAll(COMIC_MAP.values());
        NOVEL_SOURCE_LIST.addAll(NOVEL_MAP.values());
        VIDEO_SOURCE_LIST.addAll(VIDEO_MAP.values());
    }

    private static Source<EntityInfo> getCSource(int sourceId) {
        return (Source<EntityInfo>) COMIC_MAP.get(sourceId);
    }

    private static Source<EntityInfo> getNSource(int sourceId) {
        return (Source<EntityInfo>) NOVEL_MAP.get(sourceId);
    }

    private static Source<EntityInfo> getVSource(int sourceId) {
        return (Source<EntityInfo>) VIDEO_MAP.get(sourceId);
    }

    public static Source<EntityInfo> getSource(int sourceId) {
        if (Data.contentCode == Data.COMIC_CODE) {
            return (Source<EntityInfo>) COMIC_MAP.get(sourceId);
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return (Source<EntityInfo>) NOVEL_MAP.get(sourceId);
        } else {
            return (Source<EntityInfo>) VIDEO_MAP.get(sourceId);
        }
    }

    public static String getCSourceName(int sourceId) {
        Source<EntityInfo> source = (Source<EntityInfo>) COMIC_MAP.get(sourceId);
        if (source != null) {
            return source.getSourceName();
        }
        return null;
    }

    public static String getNSourceName(int sourceId) {
        Source<EntityInfo> source = (Source<EntityInfo>) NOVEL_MAP.get(sourceId);
        if (source != null) {
            return source.getSourceName();
        }
        return null;
    }

    public static String getVSourceName(int sourceId) {
        Source<EntityInfo> source = (Source<EntityInfo>) VIDEO_MAP.get(sourceId);
        if (source != null) {
            return source.getSourceName();
        }
        return null;
    }

    public static String getSourceName(int sourceId) {
        Source<EntityInfo> source = getSource(sourceId);
        if (source != null) {
            return source.getSourceName();
        }
        return null;
    }

    public static Integer getCSourceId(String name) {
        return MapUtil.getKeyByValue(COMIC_NAME_MAP, name);
    }

    public static Integer getNSourceId(String name) {
        return MapUtil.getKeyByValue(NOVEL_NAME_MAP, name);
    }

    public static Integer getVSourceId(String name) {
        return MapUtil.getKeyByValue(VIDEO_NAME_MAP, name);
    }

    public static Integer getSourceId(String name) {
        if (Data.contentCode == Data.COMIC_CODE) {
            return MapUtil.getKeyByValue(COMIC_NAME_MAP, name);
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return MapUtil.getKeyByValue(NOVEL_NAME_MAP, name);
        } else {
            return MapUtil.getKeyByValue(VIDEO_NAME_MAP, name);
        }
    }

    public static List<Source<EntityInfo>> getCSourceList() {
        return (List<Source<EntityInfo>>) (List) COMIC_SOURCE_LIST;
    }

    public static List<Source<EntityInfo>> getNSourceList() {
        return (List<Source<EntityInfo>>) (List) NOVEL_SOURCE_LIST;
    }

    public static List<Source<EntityInfo>> getVSourceList() {
        return (List<Source<EntityInfo>>) (List) VIDEO_SOURCE_LIST;
    }

    public static List<Source<EntityInfo>> getSourceList() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return getCSourceList();
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return getNSourceList();
        } else {
            return getVSourceList();
        }
    }

    public static List<String> getComicSourceNameList() {
        return COMIC_SOURCE_NAME_LIST;
    }

    public static List<String> getNovelSourceNameList() {
        return NOVEL_SOURCE_NAME_LIST;
    }

    public static List<String> getVideoSourceNameList() {
        return VIDEO_SOURCE_NAME_LIST;
    }

    public static List<String> getSourceNameList() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return COMIC_SOURCE_NAME_LIST;
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return NOVEL_SOURCE_NAME_LIST;
        } else {
            return VIDEO_SOURCE_NAME_LIST;
        }
    }

    private static int cSize() {
        return COMIC_SOURCE_LIST.size();
    }

    private static int nSize() {
        return NOVEL_SOURCE_LIST.size();
    }

    private static int vSize() {
        return VIDEO_SOURCE_LIST.size();
    }

    public static int size() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return COMIC_SOURCE_LIST.size();
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return NOVEL_SOURCE_LIST.size();
        } else {
            return VIDEO_SOURCE_LIST.size();
        }
    }

    public static EntityInfo getInfo() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return new ComicInfo();
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return new NovelInfo();
        } else {
            return new VideoInfo();
        }
    }

    public static Entity getEntity() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return new Comic();
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return new Novel();
        } else {
            return new Video();
        }
    }

    public static Entity getEntity(EntityInfo info) {
        if (Data.contentCode == Data.COMIC_CODE) {
            return new Comic((ComicInfo) info);
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return new Novel((NovelInfo) info);
        } else {
            return new Video((VideoInfo) info);
        }
    }
}
